import React from 'react';
import logo from './logo.svg';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Downloads from "../downloads/Downloads.jsx";
import FileList from "../serve/FileList.jsx";

export default class FileAwarePanel extends React.Component{
	state = {
		downloads: {
			torrents: [],
			xdcc: []
		},
		files: []
	}

	componentDidMount() {
		this.loadData();
		setInterval(this.loadData.bind(this), 10000);
	}
	
	async loadData() {
		try {
			const res = await fetch('/files');
			const data = await res.json();
			if(this.state.sensitive) {

			} else {
				this.setState({"files":data.Files});
			}
		} catch (e) {
			console.log(e);
		}
	}

	render() {
		return (<Router>
			<Route exact path={"/serve/"} render={() => {
				<FileList files={this.state.files} />
			}} />
			<Route exact path={"/serve/"} component={FileList.bind(this)} />
		</Router>);
	}
}