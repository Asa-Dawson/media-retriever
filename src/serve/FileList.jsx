import React from 'react';
import { FaLink } from 'react-icons/fa';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

export default class FileList extends React.Component {
    state = {
        target: ""
    }

    formatBytes(bytes, decimals = 2) {
        if (bytes === 0) return '0 Bytes';
    
        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    
        const i = Math.floor(Math.log(bytes) / Math.log(k));
    
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    // renderSingleVideo({match}) {
    //     return (
    //         <div className="row is-vertical-align is-horizontal-align">
    //             <video controls>
    //                 <source src={"/media/" + encodeURI(match.params.video)} />
    //             </video>
    //         </div>
    //     )
    // }

    renderFileList() {
        return (
            this.props.files.map((file) => {
                console.log(file)
                return (
                    <div className="row is-vertical-align">
                        <div className="col-10">
                            {file.Name}
                        </div>
                        <div className="col-1">
                            { file.Size ? this.formatBytes(file.Size) : "N/A" }
                        </div>
                        <div className="col-1">
                            <Link className="success button" to={"/media/" + encodeURI(file.Path)}>
                                <FaLink />
                            </Link>
                        </div>
                    </div>
                );
            })
        );
    }

    render() {
        return (
            this.renderFileList.bind(this)
        )
    }
};