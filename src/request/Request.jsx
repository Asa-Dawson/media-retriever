import React from 'react';
import ReactDOM from 'react-dom';
//import * from 'react-icons/fa';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { Form, Text, useFormState } from 'informed';

export default class Request extends React.Component {
    state = {
        torrent : {
            name: "",
            magnet: ""
        },
        xdcc: {
            name: "",
            server: "",
            port: "6667",
            channels: "",
            message: ""
        }
    }
    
    constructor(props) {
        super(props);
    }

    updateState(e) {
        let newState = {};
        newState[e.target.name] = e.target.value;
        this.setState(newState);
    }

    sendXDCCRequest(values) {
        values.channels = values.channels.replace(/\s+/g, '').split(",");
        this.makeRequest("xdcc", values);
    }

    sendTorrentRequest(values) {
        this.makeRequest("torrent", values);
    }

    makeRequest(target, requestBody) {
        console.log(target, requestBody);
        try {
            fetch(`/download/${target}`, {
                headers: {
                    'Content-type': 'application/json'
                },
                method: "post",
                body: JSON.stringify(requestBody)
            }).then(function(response) {
                console.log(response.json());
                return response.json();
            });
        } catch (err) {
            console.log(err);
        }
    }

    renderXDCCRequest() {
        return (
            <Form onSubmit={(values) => this.sendXDCCRequest(values)}>
                <label>Name<Text field="name" value={this.state.xdcc.name} /></label>
                <label>Server<Text field="server"  value={this.state.xdcc.server} /></label>
                <label>Port<Text field="port"  value={this.state.xdcc.port} /></label>
                <label>Channels<Text field="channels"  value={this.state.xdcc.channels}/></label>
                <label>Message<Text field="message"  value={this.state.xdcc.message}/></label>
                <br />
                <button type="submit" >Submit</button>
            </Form>
        );
    }

    renderTorrentRequest() {
        return (
            <Form onSubmit={(values) => this.sendTorrentRequest(values)}>
                <label>Name<Text field="name" value={this.state.torrent.name} /></label>
                <label>Magnet link<Text field="magnet" value={this.state.torrent.magnet} /></label>
                <br />
                <button type="submit" >Submit</button>
            </Form>
        );
    }

    render() {
        return (
            <fieldset>
                <legend>Make a new file request</legend>
                <Route exact path="/new/torrent" component={this.renderTorrentRequest.bind(this)} />
                <Route exact path="/new/xdcc" component={this.renderXDCCRequest.bind(this)} />
            </fieldset>
        );
    }
}