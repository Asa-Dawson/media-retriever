import React from 'react';
import logo from './logo.svg';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Nav from "./nav/Nav.jsx";
import Request from "./request/Request.jsx";
import FileAwarePanel from "./fileAwarePanel/FileAwarePanel.jsx";

class App extends React.Component {
	state = {
		downloads: {
			torrents: [],
			xdcc: []
		},
		files: []
	}

	componentDidMount() {
		this.loadData();
		setInterval(this.loadData.bind(this), 30000);
	}
	
	async loadData() {
		try {
			const res = await fetch('/files');
			const data = await res.json();

			this.setState(data);
		} catch (e) {
			console.log(e);
		}
	}

	renderHeader() {
		return (
			<div className="row">
				<div className="col-12 is-horizontal-align">
					<h1>Data Retriever</h1>
				</div>
			</div>
		)
	}

	renderDownloadPanel() {
		return (
			<Downloads files={this.state.downloads} />
		)
	}

	renderServePanel() {
		return (
		)
	}

	render() {
		return (
			<Router>
				{ this.renderHeader() }
				<Nav />
				<Route exact path="/" component={this.renderDownloadPanel.bind(this)} />
				<Route path="/new/" component={Request.bind(this)} />
				<Route path="/serve/" component={this.renderServePanel.bind(this)} />
				<FileAwarePanel />
			</Router>
		);
	}
}

export default App;
