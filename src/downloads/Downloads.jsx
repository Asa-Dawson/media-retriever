import React from 'react';
import ReactDOM from 'react-dom';
import { FaTrashAlt, FaPlayCircle } from 'react-icons/fa';
import { BrowserRouter as Link } from "react-router-dom";

export default class Downloads extends React.Component {
    constructor(props) {
        super(props);
    }

    renderDownload(file, type) {
        let seederSection = "";

        if(type === "torrent") {
            seederSection = <div className="col-2">
                [{file.seeds} | {file.peers}]
            </div>;
        }

        return (
        <div className="row is-vertical-align">
            <div className="col-2">
                {file.name}
            </div>
            <div className={`col-${type === "torrent" ? "4" : "6"}`}>
                <progress value={file.progress} max={file.size}></progress>
            </div>
            <div className="col-2">
                {file.size}
            </div>
            {seederSection}
            <div className="col-1">
                <Link className="success button" to={"/serve/" + file.location + file.name}>
                    <FaPlayCircle />
                </Link>
            </div>
            <div className="col-1">
                <Link className="error button" href="/delete/" >
                    <FaTrashAlt />
                </Link>
            </div>
            <hr />
        </div>
        );
    };
    
    renderDownloadList(list, type) {
        return (
            <details>
                <summary className="brand">{type}</summary>
                <hr />
                { list.map(file => {
                    return this.renderDownload(file, type);
                }) }
            </details>
        )
    };

    render() {
        let torrentList = this.renderDownloadList(this.props.files.torrents, "torrent");
        let xdccList = this.renderDownloadList(this.props.files.xdcc, "xdcc");
        return (
            [torrentList, xdccList]
        )
    };
};