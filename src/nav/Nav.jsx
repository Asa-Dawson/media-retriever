import React from 'react';
import { FaDog } from 'react-icons/fa';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

export default class Nav extends React.Component {
    constructor(props) {
		super(props);
	}

	renderTabButton(id, label, path) {
		let classList = `success ${this.props.currentScreen === id ? "active" : ""}`;
		return (
			<Link className={classList} id={id} to={path}>{label}</Link>
		);
	}

	render() {
		let filesButton = this.renderTabButton("files", "Files", "/");
		let serveButton = this.renderTabButton("serve", "Serve", "/serve/");
		let newTorrentButton = this.renderTabButton("newtorrent", "Add Torrent", "/new/torrent/");
		let newXDCCButton = this.renderTabButton("newxdcc", "Add XDCC", "/new/xdcc/");

		return (
			<div className="row">
				<div className="col-12">
					<div className="nav">
						<div className="nav-left">
							<div className="tabs">
								{ filesButton }
								{ serveButton }
							</div>
						</div>
						<div className="nav-center">
							<Link to="/"><h2><FaDog /></h2></Link>
						</div>
						<div className="nav-right">
							<div className="tabs">
								{ newTorrentButton }
								{ newXDCCButton }
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
};